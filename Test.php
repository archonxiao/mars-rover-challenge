<?php
/**
 * Mars Rover Challenge
 *
 * @author Qiu(Tom) Xiao
 */

require_once(__DIR__ . '/Rover.inc');
require_once(__DIR__ . '/Plateau.inc');

/**
 * Validate x and y coordinates for Plateau
 *
 * @param array
 * @return boolean
 */
function validatePlateauCoordinates($arr) {
    if (count($arr) == 2) {
        // Check if x and y are positive integers
        if ((filter_var($arr[0], FILTER_VALIDATE_INT) > 0) 
        && (filter_var($arr[1], FILTER_VALIDATE_INT) > 0)) {
            return true;
        }
    }
    return false;
}

/**
 * Validate position for Rover
 *
 * @param array
 * @return boolean
 */
function validateRoverPosition($arr) {
    if (count($arr) == 3) {
        // Check if x and y are integers and orientation is valid
        if ((filter_var($arr[0], FILTER_VALIDATE_INT) >= 0)
        && (filter_var($arr[1], FILTER_VALIDATE_INT) >= 0)
        && preg_match('/^[NSWE]$/', $arr[2])) {
            return true;
        }
    }
    return false;
}

/**
 * Validate sequence of actions for Rover
 *
 * @param array
 * @return boolean
 */
function validateRoverSequence($arr) {
    foreach ($arr as $v) {
        if(!preg_match('/^[MLR]$/', $v)) {
            return false;
        }
    }
    return true;
}

// Start test
echo "Test Input:\n";

// Read the input
$f = fopen('php://stdin', 'r');
$index = 0;

// Get input content line by line
while ($line = trim(fgets($f))) {
    $arr = explode(" ", $line);
    // Initialize Plateau by reading first input line
    if ($index == 0) {
        if (validatePlateauCoordinates($arr)) {
            $plateau = new Plateau($arr[0], $arr[1]);
            $index++;
        } else {
            echo "Invalid plateau coordinates from line #" . ($index+1) . ": " . $line ."\n";
            break;
        }
    } else {
        if (validateRoverPosition($arr)) {
            // Initialize Rover
            $rover = new Rover($arr[0], $arr[1], $arr[2]);
            $index++;
            // Read Rover's sequence of actions
            $line = trim(fgets($f));
            $arr = str_split($line);
            if (validateRoverSequence($arr)) {
                $rover->setSequence($arr);
                $plateau->addRover($rover);
                $index++;
            } else {
                echo "Invalid sequence of actions from line #" . ($index+1) . ": " . $line ."\n";
                break;
            }
        } else {
            echo "Invalid rover position from line #" . ($index+1) . ": " . $line ."\n";
            break;
        }
    }
}

// Check valid plateau and rover(s)
if (isset($plateau)) {
    if ($plateau->getRoverCount() == 0) {
        echo "No rover has been found.\n";
    } else {
        // Start exlore process
        echo "Exploration starts...\n";
        foreach ($plateau->getAllRovers() as $rover) {
            $rover->explore($plateau);
            echo $rover->getCoordinateX() . " " . $rover->getCoordinateY() . " " . $rover->getOrientation() . "\n";
        }
        echo "Exploration completed.\n";
    }
} else {
    echo "No plateau has been found.\n";
}

fclose($f);
echo ("Test End.\n");
