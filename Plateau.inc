<?php
/**
 * Mars Rover Challenge
 *
 * @author Qiu(Tom) Xiao
 */

/**
 * Plateau Class
 */
Class Plateau {
    /**
     * X coordinate
     * 
     * @var int
     */
    protected $_x = 0;

    /**
     * Y coordinate
     * 
     * @var int
     */
    protected $_y = 0;

    /**
     * Rover Count
     * 
     * @var int
     */
    protected $_roverCount = 0;

    /**
     * Rover(s) within Plateau
     * 
     * @var array
     */
    protected $_rovers = [];


    /** 
     * @param int $x
     * @param int $y
     */
    public function __construct($x, $y) {
        $this->_x = $x;
        $this->_y = $y;
    }

    /** 
     * Return x coordinate
     * 
     * @return int
     */
    public function getCoordinateX() {
        return $this->_x;
    }

    /** 
     * Return y coordinate
     * 
     * @return int
     */
    public function getCoordinateY() {
        return $this->_y;
    }

    /** 
     * Return total number of rover(s) within Plateau
     * 
     * @return int
     */
    public function getRoverCount() {
        return $this->_roverCount;
    }

    /** 
     * Return array of all rover(s) within Plateau
     * 
     * @return array
     */
    public function getAllRovers() {
        return $this->_rovers;
    }

    /**
     * Add Rover into Plateau
     *
     * @param  Rover $rover
     * @return $this
     */
    public function addRover($rover)
    {
        if ($rover->getCoordinateX() > $this->getCoordinateX()
        || $rover->getCoordinateY() > $this->getCoordinateY()) {
            echo "Rover's position is outside plateau, unable to land rover.\n";
        } else {
            $this->_rovers[] = $rover;
            $this->_roverCount++;
        }
        return $this;
    }
}