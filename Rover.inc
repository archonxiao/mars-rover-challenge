<?php
/**
 * Mars Rover Challenge
 *
 * @author Qiu(Tom) Xiao
 */

/**
 * Rover Class
 */
Class Rover {
    /**
     * X coordinate
     * 
     * @var int
     */
    protected $_x = 0;

    /**
     * Y coordinate
     * 
     * @var int
     */
    protected $_y = 0;

    /**
     * Orientation
     * 
     * @var string
     */
    protected $_o = 'N';

    /**
     * Sequence of actions
     * 
     * @var array
     */
    protected $_seq = [];

    /**
     * Degrees for orientations
     * 
     * @var array
     */
    protected $_degree = array(
        'E' => 0,
        'N' => 90,
        'W' => 180,
        'S' => 270
    );

    /** 
     * @param int $x
     * @param int $y
     */
    public function __construct($x, $y, $o) {
        $this->_x = $x;
        $this->_y = $y;
        $this->_o = $o;
    }

    /** 
     * Return x coordinate
     * 
     * @return int
     */
    public function getCoordinateX() {
        return $this->_x;
    }

    /** 
     * Return y coordinate
     * 
     * @return int
     */
    public function getCoordinateY() {
        return $this->_y;
    }

    /** 
     * Return orientation
     * 
     * @return string
     */
    public function getOrientation() {
        return $this->_o;
    }

    /** 
     * Return sequence of actions
     * 
     * @return array
     */
    public function getSequence() {
        return $this->_seq;
    }

    /**
     * Set coordinates
     *
     * @param int $x
     * @param int $y
     * @return $this
     */
    public function setCoordinates($x, $y)
    {
        $this->_x = $x;
        $this->_y = $y;
        return $this;
    }

    /**
     * Set orientation
     *
     * @param string $o
     * @return $this
     */
    public function setOrientation($o)
    {
        $this->_o = $o;
        return $this;
    }

    /**
     * Set sequence of actions
     *
     * @param array $seq
     * @return $this
     */
    public function setSequence($seq)
    {
        $this->_seq = $seq;
        return $this;
    }

    /**
     * Rover move
     *
     * @return $this
     */
    public function move()
    {
        $degree = $this->_degree[$this->getOrientation()];
        $this->setCoordinates(
            $this->getCoordinateX() + round(cos(deg2rad($degree))),
            $this->getCoordinateY() + round(sin(deg2rad($degree)))
        );
        return $this;
    }

    /**
     * Rover spin
     *
     * @param string $direction
     * @return $this
     */
    public function spin($direction)
    {
        $degree = $this->_degree[$this->getOrientation()];
        if ($direction == 'R') {
            $degree -= 90;
            if ($degree < 0) {
                $degree += 360;
            }
        } elseif ($direction == 'L') {
            $degree += 90;
            if ($degree >= 360) {
                $degree -= 360;
            }
        }
        $this->setOrientation(array_search($degree, $this->_degree));
        return $this;
    }

    /**
     * Check if Rover moves outside Plateau
     *
     * @param Plateau $plateau
     * @return boolean
     */
    public function isOutside($plateau)
    {
        if ($this->getCoordinateX() > $plateau->getCoordinateX()
        || $this->getCoordinateY() > $plateau->getCoordinateY()
        || $this->getCoordinateX() < 0
        || $this->getCoordinateY() < 0) {
            return true;
        }
        return false;
    }

    /**
     * Explore Plateau
     *
     * @param array $seq
     * @return $this
     */
    public function explore($plateau)
    {
        foreach ($this->getSequence() as $a) {
            if ($a == 'M') {
                $this->move();
                if ($this->isOutside($plateau)) {
                    echo "Rover moved outside plateau.\n";
                    break;
                }
            } else {
                $this->spin($a);
            }
        }
        return $this;
    }
}